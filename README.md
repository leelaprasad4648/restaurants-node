### Setup
 
#### App Set up 
Clone this repository and run the below command for api applications:
```
npm install

```

### Build and Run the app
App must be built each time changes are made (We don't watch in docker). 

To start the app run from the root directory:
```
npm start
```
