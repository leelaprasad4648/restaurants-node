const mongoose = require('mongoose');
const RestaurantSchema = mongoose.Schema({
    name: String,
    type: String,
    description: String,
    address: String
}, {
    timestamps: true
});
module.exports = mongoose.model('Note', RestaurantSchema);