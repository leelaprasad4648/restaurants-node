const Restaurant = require('../models/restaurants.model.js');


// Add a Restaurant to DB
exports.create = (req, res) => {

    console.log("req is " + req.body.name + "  and  " + req.body.type);
    // Validate request
    if (!req.body.name) {
        return res.status(400).send({
            message: "Please enter a Restaurant Name"
        });
    }
    if (!req.body.type) {
        return res.status(400).send({
            message: "Please enter a Restaurant type"
        });
    }

    if (!req.body.address) {
        return res.status(400).send({
            message: "Please enter address"
        });
    }

    //Create a Restaurant
    const restaurant = new Restaurant({
        name: req.body.name,
        type: req.body.type,
        description: req.body.description || "No description",
        address: req.body.address
    });

    // Save Restaurant in the database
    restaurant.save()
        .then(data => {
            res.send(data);
        }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while adding the Restaurant"
        });
    });
};

// Retrieve and return all Restaurant from the database.
exports.findAll = (req, res) => {
    Restaurant.find()
        .then(restaurants => {
            res.send(restaurants);
        }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving Restaurant List."
        });
    });
};

// Find a single Restaurant with a restaurantId
exports.findOne = (req, res) => {
    Restaurant.findById(req.params.restaurantId)
        .then(restaurant => {
            if (!restaurant) {
                return res.status(404).send({
                    message: "Restaurant not found with id " + req.params.restaurantId
                });
            }
            res.send(restaurant);
        }).catch(err => {
        if (err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Restaurant not found with id " + req.params.restaurantId
            });
        }
        return res.status(500).send({
            message: "Error retrieving Restaurant with id " + req.params.restaurantId
        });
    });
};

// Update a Restaurant identified by the restaurantId in the request
exports.update = (req, res) => {
    // Validate Request
    if (!req.body.name) {
        return res.status(400).send({
            message: "Please enter a Restaurant Name"
        });
    }
    if (!req.body.type) {
        return res.status(400).send({
            message: "Please enter a Restaurant type"
        });
    }

    if (!req.body.address) {
        return res.status(400).send({
            message: "Please enter address"
        });
    }
    // Find Restaurant and update it with the request body
    Restaurant.findByIdAndUpdate(req.params.restaurantId, {
        name: req.body.name,
        type: req.body.type,
        description: req.body.description || "No description",
        address: req.body.address
    }, {new: true})
        .then(restaurant => {
            if (!restaurant) {
                return res.status(404).send({
                    message: "Restaurant not found with id " + req.params.restaurantId
                });
            }
            res.send(restaurant);
        }).catch(err => {
        if (err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Restaurant not found with id " + req.params.restaurantId
            });
        }
        return res.status(500).send({
            message: "Error updating Restaurant with id " + req.params.restaurantId
        });
    });
};

// Delete a Restaurant with the specified restaurantId in the request
exports.delete = (req, res) => {
    Restaurant.findByIdAndRemove(req.params.restaurantId)
        .then(restaurant => {
            if (!restaurant) {
                return res.status(404).send({
                    message: "Restaurant not found with id " + req.params.restaurantId
                });
            }
            res.send({message: "Restaurant deleted successfully from the Restaurant List!"});
        }).catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Restaurant not found with id " + req.params.noteId
            });
        }
        return res.status(500).send({
            message: "Could not delete Restaurant with id " + req.params.noteId
        });
    });
};