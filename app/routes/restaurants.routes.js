module.exports = (app) => {

    const restaurants = require('../controllers/restaurants.controller.js');

    // Add a Restaurant to Restaurant List
    app.post('/restaurants', restaurants.create);

    // Retrieve all Restaurants from restaurantList
    app.get('/restaurants', restaurants.findAll);

    // Retrieve a single restaurant with restaurantId
    app.get('/restaurants/:restaurantId', restaurants.findOne);

    // Update a restaurant with restaurantId
    app.put('/restaurants/:restaurantId', restaurants.update);

    // Delete a restaurant with restaurantId
    app.delete('/restaurants/:restaurantId', restaurants.delete);
};
